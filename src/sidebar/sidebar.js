import { MessageTypes } from "../message-types.js";

// on sidebar open
document.addEventListener("DOMContentLoaded", function() {
    browser.runtime.sendMessage({
        type: MessageTypes.SidebarOpened
    }); 
});
// on sidebar close
document.addEventListener("unload", function() {
    browser.runtime.sendMessage({
        type: MessageTypes.SidebarClosed
    }); 
});

function handleMessage(request) {
    console.log(`sidebar handleMessage(${JSON.stringify(request.type)})`);
    switch (request.type) {
        case MessageTypes.RenderTitle:
            document.getElementById('title').innerHTML = request.body.message ?? '---'
            break;
            
        case MessageTypes.RenderLLMReply:
            document.getElementById('summary').innerHTML = request.body.message ?? '---'
            break;
        }
}
// listen for messages from backgroud script
browser.runtime.onMessage.addListener(handleMessage);