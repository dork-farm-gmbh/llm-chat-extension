import { MessageTypes } from "./message-types.js";

console.log('Hello from bacgkorund');


async function handleMessage(request) {
    console.log(`handleMessage(${JSON.stringify(request.type)})`);
    switch (request.type) {
        case MessageTypes.SidebarOpened:
            async function logTabs(tabs) {
                let tabId = tabs[0].id
                await browser.scripting.executeScript({
                    target: {
                        tabId
                    },
                    files: ["dependencies/Readability.js", "content.js"],
                });
            }
            function onError(error) {
                console.error(`Error: ${error}`);
            }
    
            browser.tabs
                .query({})
                .then(logTabs, onError);
            break;
        case MessageTypes.ArticleLoaded:
            // // validate article
            // if(!request.body.title || !request.body.textContent)
            browser.runtime.sendMessage({
                type: MessageTypes.RenderTitle,
                body: {message: request.body.title}
            }); 

            // summarize the article
            browser.runtime.sendMessage({
                type: MessageTypes.RenderLLMReply,
                body: {message: request.body.textContent}
            }); 

            break;

    }
}

browser.runtime.onMessage.addListener(handleMessage);

console.log('END from bacgkorund');

