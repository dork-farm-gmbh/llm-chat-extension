// import { MessageTypes } from "./message-types.js";

const documentClone = document.cloneNode(true); 
const article = new Readability(documentClone).parse();
console.log("🚀 ~ article:", article.title)
browser.runtime.sendMessage({
    type: 'article-loaded',
    body: article
});
