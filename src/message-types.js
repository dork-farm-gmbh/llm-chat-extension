export const MessageTypes = {
    SidebarOpened: 'sidebar-opened',
    SidebarClosed: 'sidebar-closed',
    ArticleLoaded: 'article-loaded',
    RenderTitle: 'render-title',
    RenderLLMReply: 'render-llm-reply',
}